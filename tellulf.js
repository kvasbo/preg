var days = 282;
var weeks = 40;

var version = 5;

var checkTimer;

var bgcolor = "#CCFFCC";

var sidesize = 10;

var dayMilliSecs = 86400000;
var weekMilliSecs = 604800000; 

var start = new Date(2014, 9, 14, 0, 0, 0);
var startStupid = new Date(2014, 9, 1, 0, 0, 0);
var end = new Date(2015, 6, 10, 0, 0, 0);
var length = end - start;

CoolClock.config.skins = {

	audun: {
		outerBorder:      { lineWidth: 4, radius: 97, color: "black", alpha: 1 },
		smallIndicator:   { lineWidth: 0, startAt: 90, endAt: 94, color: "black", alpha: 0 },
		largeIndicator:   { lineWidth: 0, startAt: 89, endAt: 94, color: "black", alpha: 1 },
		hourHand:         { lineWidth: 8, startAt: 0, endAt: 60, color: "black", alpha: 1 },
		minuteHand:       { lineWidth: 8, startAt: 0, endAt: 80, color: "black", alpha: 1 },
		secondHand:       { lineWidth: 6, startAt: 81, endAt: 87, color: "red", alpha: 1 },
		secondDecoration: { lineWidth: 3, startAt: 0, radius: 4, fillColor: "black", color: "black", alpha: 0 }
		}
	};

function updatePreg()
{
	var now = new Date();
	var p = getPercent(now);
	
	var outstr;
	if(p < 100)
	{
		outStr = p.toFixed(5).toString()+"%";
	}
	else
	{
		outStr = p.toFixed(4).toString()+"%";
	}

	if($("#percent").html() !== outStr)
	{
		$("#percent").html(outStr);
	}
	
	var week = getWeek(now);
	var day = getDays(now);
	
	var dayOfWeek = (day % 7);

	var outStrW = "uke " + week + " (" + dayOfWeek  + ")";

	if($("#week").html() !== outStrW)
	{
		$("#week").html(outStrW);
	}


	var weight = getWeight(week,dayOfWeek);
	var height = getHeight(week,dayOfWeek);

	if(weight !== null && height !== null)
	{
		$("#height").html(height+" cm");
		$("#weight").html(weight+" g");
	}

	setTimeout(updatePreg,500);	

}

function updateWeather()
{
	$.getJSON("http://www.kvasbo.no/temp/getTemps.php", function(data){

		if(typeof(data["error"] !== "undefined") && data["error"] === "0")
		{

			//console.log(data);

			var strU = "<br>Ute<br>"+data["outside"]["temp"] + "&deg;C<br>" + data["outside"]["hum"] + "%";
			var strI = "Inne<br>"+data["inside"]["temp"] + "&deg;C<br>" + data["inside"]["hum"] + "%<br>"+data["inside"]["co2"]+"ppm CO2<br>"+data["inside"]["pressure"]+"mbar";

			//var strI = "I: "+data["inside"]["hum"]+ "% / " + data["inside"]["temp"] + "";

			$("#ute").html(strU);
			$("#inne").html(strI);

		}

	});

	setTimeout(updateWeather, 60000);
}

window.onload = function(e){ 

	var reloadTimer = setTimeout(reload, 1*60*60*1000);
	checkTimer = setTimeout(checkUpdates, 1000); //Check after 1 sec
	updatePreg();
	updateWeather();

}