var days = 282;
var weeks = 40;

var bgcolor = "#CCFFCC";

var sideSize = 0;

var dayMilliSecs = 86400000;
var weekMilliSecs = 604800000; 

var start = new Date(2014, 9, 14, 0, 0, 0);
var startStupid = new Date(2014, 9, 1, 0, 0, 0);
var end = new Date(2015, 6, 10, 0, 0, 0);
var length = end - start;

function updateSlow()
{
	var now = new Date();
	var p = getPercent(now);
	var week = getWeek(now);
	var day = getDays(now);
	var dayOfWeek = (day % 7);

	var weight = getWeight(week, dayOfWeek);
	var height = getHeight(week, dayOfWeek);

	if(weight !== null && height !== null)
	{
		var hwString = weight.toString() + "g / " + height.toString() + "cm"
		$("#days").html(hwString);
	}

	updateColorGrid();

	

	

	$("#week").html("uke " + week + " (" + dayOfWeek  + ")");

	setTimeout(updateSlow, 60000);
}

function updateFast()
{
	var now = new Date();
	var p = getPercent(now);
	
	document.getElementById("percent").innerHTML = p.toFixed(6).toString()+"%";
	
	setTimeout(updateFast,42);
}


function updateColorGrid()
{
	var now = new Date();
	var doneDays = getDays(now);
	var percentOfDay = (((now.getHours()*3600) + (now.getMinutes() * 60) + now.getSeconds()) / 86400).toFixed(3);



	var widthOfTodayFill = Math.round(percentOfDay * sidesize);

	var sel = "#calD"+doneDays;

	$(sel).html("");

	jQuery('<div/>', {
	    style: 'background-color: '+bgcolor+'; height: '+sidesize+'; width: '+widthOfTodayFill+';',
	    id: "todayProgress"
	}).appendTo(sel);

	

	for(i=14;i<doneDays;i++)
	{
		var sel = "#calD"+i;
		$(sel).css("background-color", bgcolor);
	}
}

function initGrid()
{
	
	$("#cal").html("");
	
	for(i=14;i<=days;i++)
	{


		var calD = new Date(2014, 9, 1+i-1, 0, 0, 0);

		var selId = "calD" + i;

		var dateStr = calD.getDate() + "." + (calD.getMonth() + 1).toString();

		var str = "<div class='calCell' id='"+selId+"'></div>";

		$("#cal").append(str);

		if(i%7 == 0)
		{
			var w = getWeight(i/7,0);
			var h = getHeight(i/7,0);
			$("#"+selId).append((i/7).toString()+"<br/>"+dateStr.toString()+"<br/>"+w+"g<br/>"+h+"cm");
		}

		if(i/7 == 13)
		{
			//$("#"+selId).addClass("t1");
		}
		else if(i/7 == 27)
		{
			//$("#"+selId).addClass("t1");
		}

	}

	var pixels = window.innerWidth * window.innerHeight;
	
	sidesize = Math.floor(Math.sqrt(pixels/(days-15)) * 0.9);

	var rest = Math.abs((window.innerWidth % (sidesize+1))-20);

	$(".calCell").css("width", sidesize).css("height", sidesize).css("margin", "auto");

	$("#cal").css("padding-left", rest);



	updateColorGrid();
}



function writeHidden()
{

	var now = new Date();
	var p = Math.ceil(getPercent(now));

	$("#hidden").html("");
	var c = 0;

	for(i=p;i<100;i++)
	{
		c++;
		var t = getTimeForPercent(i/100);


		var ts = padText(t.getDate()) + "." + padText((t.getMonth()+1)) + " " + padText(t.getHours()) + ":" + padText(t.getMinutes());

		$("#hidden").append(i+"%: "+ts+" ");
		if(c%10==0) $("#hidden").append("<br/>");
	}

}

window.onload = function(e){ 
	setTimeout(reload, 60*60*1000);

 	initGrid();
 	updateFast();
 	updateSlow();
 	writeHidden();


 	$("#week").on("click", function(){reload();});

}

$(window).on("keypress", function(event)
{
	if(event.charCode==112)
		{
			$("#hidden").fadeToggle();
		}
});

$(window).resize(function() {
  	initGrid();
});