var fweight = [1,2,4,7,14,23,43,70,100,140,190,240,300,360,420,500,580,660,760,875,1005,1153,1319,1502,1702,1918,2146,2383,2622,2859,3083,3288,3462,3597,3685];
var fheight = [1.6,2.3,3.1,4.1,5.4,7.4,8.7,10.1,11.6,13,14.2,15.3,16.4,26.7,27.8,28.9,30,34.6,35.6,36.6,37.6,38.6,39.9,41.1,42.4,43.7,45,46.2,47.4,48.6,49.8,50.7,51.2,51.7,51.7]

function getHeight(week, day)
{
	var r;

	if(week == 20) //Special case
	{
		var base = fheight[week-8];

		r = base + (day * 0.2);

		return r.toFixed(1);
	}
	else if(week >= 8 && week < 42)
	{
		var base = fheight[week-8];
		var next = (fheight[week-7] - base) * (day / 7);

		r = (base + next);

		return r.toFixed(1);
		
	}
	else
	{
		return null;
	}
}

function getWeight(week,day)
{

	var r; 

	if(week >= 8 && week < 42)
	{
		var base = fweight[week-8];
		var next = (fweight[week-7] - base) * (day / 7);

		r = (base + next);
		
		if(r > 10)
		{
			return r.toFixed(0);
		}
		else
		{
			return r.toFixed(1);
		}
		
	}
	else
	{
		return null;
	}

}

/*
Gestational age	Length (US)	Weight (US)	Length (cm)	Mass (g)
 	(crown to rump)	(crown to rump)
8 weeks	0.63 inch	0.04 ounce	1.6 cm	1 gram
9 weeks	0.90 inch	0.07 ounce	2.3 cm	2 grams
10 weeks	1.22 inch	0.14 ounce	3.1 cm	4 grams
11 weeks	1.61 inch	0.25 ounce	4.1 cm	7 grams
12 weeks	2.13 inches	0.49 ounce	5.4 cm	14 grams
13 weeks	2.91 inches	0.81 ounce	7.4 cm	23 grams
14 weeks	3.42 inches	1.52 ounce	8.7 cm	43 grams
15 weeks	3.98 inches	2.47 ounces	10.1 cm	70 grams
16 weeks	4.57 inches	3.53 ounces	11.6 cm	100 grams
17 weeks	5.12 inches	4.94 ounces	13 cm	140 grams
18 weeks	5.59 inches	6.70 ounces	14.2 cm	190 grams
19 weeks	6.02 inches	8.47 ounces	15.3 cm	240 grams
20 weeks	6.46 inches	10.58 ounces	16.4 cm	300 grams
 	(crown to heel)	(crown to heel)
20 weeks	10.08 inches	10.58 ounces	25.6 cm	300 grams
21 weeks	10.51 inches	12.70 ounces	26.7 cm	360 grams
22 weeks	10.94 inches	15.17 ounces	27.8 cm	430 grams
23 weeks	11.38 inches	1.10 pound	28.9 cm	501 grams
24 weeks	11.81 inches	1.32 pound	30 cm	600 grams
25 weeks	13.62 inches	1.46 pound	34.6 cm	660 grams
26 weeks	14.02 inches	1.68 pound	35.6 cm	760 grams
27 weeks	14.41 inches	1.93 pound	36.6 cm	875 grams
28 weeks	14.80 inches	2.22 pounds	37.6 cm	1005 grams
29 weeks	15.2 inches	2.54 pounds	38.6 cm	1153 grams
30 weeks	15.71 inches	2.91 pounds	39.9 cm	1319 grams
31 weeks	16.18 inches	3.31 pounds	41.1 cm	1502 grams
32 weeks	16.69 inches	3.75 pounds	42.4 cm	1702 grams
33 weeks	17.20 inches	4.23 pounds	43.7 cm	1918 grams
34 weeks	17.72 inches	4.73 pounds	45 cm	2146 grams
35 weeks	18.19 inches	5.25 pounds	46.2 cm	2383 grams
36 weeks	18.66 inches	5.78 pounds	47.4 cm	2622 grams
37 weeks	19.13 inches	6.30 pounds	48.6 cm	2859 grams
38 weeks	19.61 inches	6.80 pounds	49.8 cm	3083 grams
39 weeks	19.96 inches	7.25 pounds	50.7 cm	3288 grams
40 weeks	20.16 inches	7.63 pounds	51.2 cm	3462 grams
41 weeks	20.35 inches	7.93 pounds	51.7 cm	3597 grams
42 weeks	20.28 inches	8.12 pounds	51.5 cm	3685 grams
*/