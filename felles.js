function getPercent(now)
{
	var done = now - start;
	return (done / length) * 100;
}

function getWeek(now)
{
	var nowForDate = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 12,0,0);
	var timePassed = (nowForDate - startStupid) + dayMilliSecs;
	var weeksPassed = Math.floor(timePassed/weekMilliSecs);
	return weeksPassed; 
}

function getDays(now)
{
	var nowForDate = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 12,0,0);
	var timePassed = nowForDate - startStupid; 
	var daysPassed = 1 + Math.floor(timePassed/86400000);
	return daysPassed; 
}

function getTimeForPercent(percent)
{
  	var x = percent * length;
  	var timestamp = Math.round(x + start.getTime());
  	return new Date(timestamp);
}

function reload()
{
	location.reload(true);
}

function padText(text)
{
	text = text.toString();
	if(text.length < 2)
	{
		text = "0"+text;
	}
	return text;
}